-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2021 at 08:06 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portal_kecamatan`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota_keluarga`
--

CREATE TABLE `anggota_keluarga` (
  `id` int(11) NOT NULL,
  `id_data` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `input_by` varchar(100) NOT NULL,
  `nik` varchar(100) NOT NULL,
  `tempat_lahir` date NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(100) NOT NULL,
  `agama` varchar(100) NOT NULL,
  `jenis_pekerjaan` varchar(100) NOT NULL,
  `status_perkawinan` varchar(100) NOT NULL,
  `status_hub_keluarga` varchar(100) NOT NULL,
  `kewarganegaraan` varchar(100) NOT NULL,
  `no_passpor` varchar(100) NOT NULL,
  `no_kitap` varchar(100) NOT NULL,
  `nama_ayah` varchar(100) NOT NULL,
  `nama_ibu` varchar(100) NOT NULL,
  `pendidikan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `buat_kk`
--

CREATE TABLE `buat_kk` (
  `id` int(11) NOT NULL,
  `id_data` varchar(100) NOT NULL,
  `status_kependudukan` varchar(100) NOT NULL,
  `asal_kecamatan` varchar(100) NOT NULL,
  `alasan` varchar(100) NOT NULL,
  `kelurahan` varchar(100) NOT NULL,
  `input_by` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `surat_pindah` varchar(100) NOT NULL,
  `surat_izin_menetap` varchar(100) NOT NULL,
  `surat_kehilangan` varchar(100) NOT NULL,
  `pass_photo` varchar(100) NOT NULL,
  `tanggal_input` date NOT NULL DEFAULT current_timestamp(),
  `keterangan` varchar(100) NOT NULL,
  `tanggal_proses` date DEFAULT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `data_kia`
--

CREATE TABLE `data_kia` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nik` varchar(100) NOT NULL,
  `nokk` varchar(100) NOT NULL,
  `tpt_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jns_kelamin` varchar(100) NOT NULL,
  `agama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kelurahan` varchar(100) NOT NULL,
  `ktpo` varchar(100) NOT NULL,
  `fc_kk` varchar(100) NOT NULL,
  `pass_photo` varchar(100) NOT NULL,
  `akta` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `id_data` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tanggal_input` date NOT NULL DEFAULT current_timestamp(),
  `tanggal_proses` date NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Baru',
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `data_ktp`
--

CREATE TABLE `data_ktp` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nik` varchar(50) NOT NULL,
  `nokk` varchar(50) NOT NULL,
  `tpt_lahir` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jns_kelamin` varchar(50) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `agama` varchar(100) NOT NULL,
  `status_kawin` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `kelurahan` varchar(100) NOT NULL,
  `surat_p_rt` varchar(100) NOT NULL,
  `surat_khl` varchar(100) NOT NULL,
  `fc_kk` varchar(100) NOT NULL,
  `pass_photo` varchar(100) NOT NULL,
  `tanggal_input` date NOT NULL DEFAULT current_timestamp(),
  `status` varchar(100) NOT NULL DEFAULT 'Baru',
  `keterangan` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `id_data` varchar(100) NOT NULL,
  `tgl_update` date DEFAULT NULL,
  `tanggal_proses` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `data_skck`
--

CREATE TABLE `data_skck` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nik` varchar(100) NOT NULL,
  `nokk` varchar(100) NOT NULL,
  `tpt_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jns_kelamin` varchar(100) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `agama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `keperluan` varchar(100) NOT NULL,
  `pengantar_rt` varchar(100) NOT NULL,
  `fc_kk` varchar(100) NOT NULL,
  `fc_ktp` varchar(100) NOT NULL,
  `pass_photo` varchar(100) NOT NULL,
  `input_by` varchar(100) NOT NULL,
  `id_data` varchar(100) NOT NULL,
  `tanggal_input` date NOT NULL DEFAULT current_timestamp(),
  `status` varchar(100) NOT NULL DEFAULT 'Baru',
  `keterangan` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tanggal_proses` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `data_spd`
--

CREATE TABLE `data_spd` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nik` varchar(100) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `alamat_asal` varchar(100) NOT NULL,
  `fc_kk` varchar(100) NOT NULL,
  `fc_ktp` varchar(100) NOT NULL,
  `surat_pindah` varchar(100) NOT NULL,
  `input_by` varchar(100) NOT NULL,
  `id_data` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Baru',
  `tanggal_input` date NOT NULL DEFAULT current_timestamp(),
  `tanggal_proses` date NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `data_spk`
--

CREATE TABLE `data_spk` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nik` varchar(100) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `alamat_asal` varchar(100) NOT NULL,
  `alamat_tujuan` varchar(100) NOT NULL,
  `fc_kk` varchar(100) NOT NULL,
  `fc_ktp` varchar(100) NOT NULL,
  `surat_pengantar` varchar(100) NOT NULL,
  `input_by` varchar(100) NOT NULL,
  `id_data` varchar(100) NOT NULL,
  `tanggal_proses` date NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Baru',
  `email` varchar(100) NOT NULL,
  `tanggal_input` date NOT NULL DEFAULT current_timestamp(),
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `keluhan`
--

CREATE TABLE `keluhan` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `konten` text NOT NULL,
  `username` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id_pengumuman` int(11) NOT NULL,
  `judul` varchar(500) NOT NULL,
  `isi` text NOT NULL,
  `penulis` varchar(100) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`id_pengumuman`, `judul`, `isi`, `penulis`, `tanggal`) VALUES
(1, 'Sample Berita', '<p>Teknologi film sebagian besar muncul dari perkembangan dan pencapaian di bidang proyeksi, lensa, fotografi, dan optik. Teknik awal yang melibatkan gambar bergerak atau proyeksi meliputi:</p>\r\n\r\n<ul>\r\n	<li>Shadowgraphy (dalam praktik sejak zaman prasejarah)</li>\r\n	<li>Kamera obscura (fenomena alam yang mungkin telah digunakan sebagai alat bantu seni sejak zaman prasejarah)</li>\r\n	<li>Wayang kulit (mungkin berasal sekitar 200 SM di Asia Tengah, India, Indonesia atau Cina)</li>\r\n	<li>Lampu sorot (dikembangkan pada 1650-an, didahului oleh beberapa proyektor insidental)</li>\r\n	<li>Perangkat animasi stroboskopik (fenakistoskop sejak 1833, zoetrope sejak 1866, flip book sejak 1868)</li>\r\n</ul>\r\n\r\n<p>Sepanjang sisa abad ke-19, para penemu mencoba untuk menggabungkan semua teknik tersebut untuk menciptakan film.</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n', 'Admin Admin', '2021-07-17'),
(3, 'hasemane', '<p>Teknologi film sebagian besar muncul dari perkembangan dan pencapaian di bidang proyeksi, lensa, fotografi, dan optik. Teknik awal yang melibatkan gambar bergerak atau proyeksi meliputi:</p>\r\n\r\n<ul>\r\n	<li>Shadowgraphy (dalam praktik sejak zaman prasejarah)</li>\r\n	<li>Kamera obscura (fenomena alam yang mungkin telah digunakan sebagai alat bantu seni sejak zaman prasejarah)</li>\r\n	<li>Wayang kulit (mungkin berasal sekitar 200 SM di Asia Tengah, India, Indonesia atau Cina)</li>\r\n	<li>Lampu sorot (dikembangkan pada 1650-an, didahului oleh beberapa proyektor insidental)</li>\r\n	<li>Perangkat animasi stroboskopik (fenakistoskop sejak 1833, zoetrope sejak 1866, flip book sejak 1868)</li>\r\n</ul>\r\n\r\n<p>Sepanjang sisa abad ke-19, para penemu mencoba untuk menggabungkan semua teknik tersebut untuk menciptakan film.</p>\r\n', 'Admin Admin', '2021-07-17');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` varchar(100) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `nik` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `aktif` varchar(100) NOT NULL DEFAULT '0',
  `tgl_lahir` varchar(100) NOT NULL,
  `tpt_lahir` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `username`, `password`, `level`, `foto`, `nik`, `email`, `token`, `aktif`, `tgl_lahir`, `tpt_lahir`) VALUES
(11, 'diablo', 'sueb', 'diablo', '65829e542dd151f443cc997270c61e78c042a82d687cc13844bf2c1813714600', 'admin', 'POFILE1490454377.jpg', '320192377126876', 'saepulanwar.ac@gmail.com', 'a775c674e0713c8bf2b257aa00c92783e219a629e775cf2b46dbcef0a704e0e8', '1', '2021-07-30', 'sukabumi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota_keluarga`
--
ALTER TABLE `anggota_keluarga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buat_kk`
--
ALTER TABLE `buat_kk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_kia`
--
ALTER TABLE `data_kia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_ktp`
--
ALTER TABLE `data_ktp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_skck`
--
ALTER TABLE `data_skck`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_spd`
--
ALTER TABLE `data_spd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_spk`
--
ALTER TABLE `data_spk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keluhan`
--
ALTER TABLE `keluhan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota_keluarga`
--
ALTER TABLE `anggota_keluarga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `buat_kk`
--
ALTER TABLE `buat_kk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_kia`
--
ALTER TABLE `data_kia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_ktp`
--
ALTER TABLE `data_ktp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `data_skck`
--
ALTER TABLE `data_skck`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_spd`
--
ALTER TABLE `data_spd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_spk`
--
ALTER TABLE `data_spk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `keluhan`
--
ALTER TABLE `keluhan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
