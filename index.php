<?php
ob_start();
session_start();
if(isset($_SESSION['akun_username'])){

    if((time() - $_SESSION['last_login_time']) > 900){
        header("location:logout.php");
    }else{
        $_SESSION['last_login_time']=time();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Portal Kecamatan</title>
    <link rel="shortcut icon" href="./assets/img/favicon.png" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="css/styles.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <!-- Navbar Brand-->
        <a class="navbar-brand ps-3" href="/portal_kecamatan">PORTAL KECAMATAN</a>
        <!-- Sidebar Toggle-->
        <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
        <!-- Navbar Search-->
        <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
            <div class="input-group">
                <!-- <input class="form-control" type="text" placeholder="Search for..." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
                <button class="btn btn-primary" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button> -->
            </div>
        </form>
        <!-- Navbar-->
        
        <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
        <?php if(isset($_SESSION['akun_username'])){?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="?page=profile">Profile</a></li>
                    <li><a class="dropdown-item" href="#!">Privilege : <?= $_SESSION['akun_level'];?></a></li>
                    <li>
                        <hr class="dropdown-divider" />
                    </li> 
                    <li><a class="dropdown-item" href="logout.php">Logout</a></li>
                </ul>
            </li>
            <?php }else{?>
                <li><a href="login.php">Login</a></li>
                <?php }?>
        </ul>
    </nav>

    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <!-- <div class="sb-sidenav-menu-heading">Core</div> -->
                        <a class="nav-link" href="/portal_kecamatan">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Home
                        </a>
                        <a class="nav-link" href="?page=pengaduan">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Pengaduan & Saran
                        </a>
                        <?php if($_SESSION['akun_level'] == "admin"){?>
                            <a class="nav-link" href="?page=user">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Users
                        </a>
                        <a class="nav-link" href="?page=report">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Report
                        </a>
                        <?php } ?>
                        <!-- <div class="sb-sidenav-menu-heading">Interface</div> -->
                        <?php if($_SESSION['akun_level'] != "admin"){?>
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                            <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                            Form
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="?page=buatKTP">E-KTP</a>
                                <a class="nav-link" href="?page=buatKK">Kartu Keluarga</a>
                                <a class="nav-link" href="?page=buatKIA">Kartu Identitas Anak</a>
                                <a class="nav-link" href="?page=buatSKCK">Surat Pengantar SKCK</a>
                                <a class="nav-link" href="?page=buatSPK">Surat Pindah Keluar</a>
                                <a class="nav-link" href="?page=buatSPD">Surat Pindah Datang</a>
                            </nav>
                        </div>
                        <?php } ?>
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                            <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                            Data
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-bs-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="?page=dataKTP">E-KTP</a>
                                <a class="nav-link" href="?page=dataKK">Kartu Keluarga</a>
                                <a class="nav-link" href="?page=dataKIA">Kartu Identitas Anak</a>
                                <a class="nav-link" href="?page=dataSKCK">Surat Pengantar SKCK</a>
                                <a class="nav-link" href="?page=dataSPK">Surat Pindah Keluar</a>
                                <a class="nav-link" href="?page=dataSPD">Surat Pindah Datang</a>
                            </nav>
                        </div>
                        
                       
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small"><?php if(isset($_SESSION['akun_username'])){ echo "Logged in as: ". $_SESSION['akun_username'];}?></div>

                    </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid px-4">
                    <h3 class="mt-4">
                        <?php
                        $page = @$_GET['page'];
                        if ($page == "") {
                            echo "HOME";
                        } elseif ($page == "buatKTP") {
                            echo "E-KTP";
                        } elseif ($page == "buatKK") {
                            echo "KARTU KAELUARGA";
                        } elseif ($page == "buatKIA") {
                            echo "KARTU IDENTITAS ANAK";
                        } elseif ($page == "buatSKCK") {
                            echo "SURAT PENGANTAR SKCK";
                        } elseif ($page == "buatSPK") {
                            echo "SURAT PINDAH KELUAR";
                        } elseif ($page == "buatSPD") {
                            echo "SURAT PINDAH DATANG";
                        }elseif ($page == "dataKTP") {
                            echo "E-KTP";
                        } elseif ($page == "dataKK") {
                            echo "KARTU KAELUARGA";
                        } elseif ($page == "dataKIA") {
                            echo "KARTU IDENTITAS ANAK";
                        } elseif ($page == "dataSKCK") {
                            echo "SURAT PENGANTAR SKCK";
                        } elseif ($page == "dataSPK") {
                            echo "SURAT PINDAH KELUAR";
                        } elseif ($page == "dataSPD") {
                            echo "SURAT PINDAH DATANG";
                        } elseif ($page == "pengaduan") {
                            echo "PENGADUAN & SARAN";
                        }elseif ($page == "profile") {
                            echo "USER PROFILE";
                        }elseif ($page == "user") {
                            echo "LIST AKUN";
                        }elseif ($page == "report") {
                            echo "REPORT";
                        }
                        ?>
                    </h3>
                    <h6>
                        <?php
                        $page = @$_GET['page'];
                        if ($page == "") {
                            echo "HOME";
                        } elseif ($page == "buatKTP") {
                            echo "Formulir E-KTP";
                        } elseif ($page == "buatKK") {
                            echo "Formulir KK";
                        } elseif ($page == "buatKIA") {
                            echo "Formulir KIA";
                        } elseif ($page == "buatSKCK") {
                            echo "Formulir Pengantar SKCK";
                        } elseif ($page == "buatSPD") {
                            echo "Formulir SPD";
                        } elseif ($page == "buatSPK") {
                            echo "Formulir SPK";
                        }elseif ($page == "dataKTP") {
                            echo "Data E-KTP";
                        } elseif ($page == "dataKK") {
                            echo " Data KK";
                        } elseif ($page == "dataKIA") {
                            echo "Data KIA";
                        } elseif ($page == "dataSKCK") {
                            echo "Data Pengantar SKCK";
                        } elseif ($page == "dataSPD") {
                            echo "Data SPD";
                        } elseif ($page == "dataSPK") {
                            echo "Data SPK";
                        } elseif ($page == "pengaduan") {
                     
                        }
                        ?>
                    </h6>
                    <div class="container">
                        <?php
                        $page = @$_GET['page'];
                        if ($page == "") {
                            include "home.php";
                        } elseif ($page == "buatKTP") {
                            include "buatKTP.php";
                        } elseif ($page == "buatKK") {
                            include "buatKK.php";
                        } elseif ($page == "buatKIA") {
                            include "buatKIA.php";
                        } elseif ($page == "buatSKCK") {
                            include "buatSKCK.php";
                        } elseif ($page == "buatSPD") {
                            include "buatSPD.php";
                        } elseif ($page == "buatSPK") {
                            include "buatSPK.php";
                        }elseif ($page == "dataKTP") {
                            include "dataEKTP.php";
                        } elseif ($page == "dataKK") {
                            include "dataKK.php";
                        } elseif ($page == "dataKIA") {
                            include "dataKIA.php";
                        } elseif ($page == "dataSKCK") {
                            include "dataSKCK.php";
                        } elseif ($page == "dataSPD") {
                            include "dataSPD.php";
                        } elseif ($page == "dataSPK") {
                            include "dataSPK.php";
                        }else if($page == "tulisArtikel"){
                            include "tulisArtikel.php";
                        }else if($page == "pengaduan"){
                        include "pengaduan.php";
                        }else if($page == "profile"){
                            include "profile.php";
                        }else if($page == "user"){
                            include "user.php";
                        }else if($page == "report"){
                            include "report.php";
                        }
                        ?>
                    </div>
                </div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid px-4">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Rancangan Tugas Akhir 2021</div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script> -->
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="js/datatables-simple-demo.js"></script>
</body>
</html>
<?php }else{
 header("location:login.php");   
}?>