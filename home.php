<?php
ob_start();
// BUAT KONEKSI KE DATABASE
include('koneksi.php');

$sql= "SELECT * FROM pengumuman ORDER BY id_pengumuman DESC LIMIT 5";
$result = $conn->query($sql);
$result;

?>

<style>
.content{
    text-align: justify;
}
.flex{
    width: 100%;
    display: grid;
    grid-template-areas: "artikel berita";
    grid-template-columns: 3fr 1fr;
}
.kiri{
    grid-area: artikel;

}
.kanan{
    grid-area: berita;
}


     /* .info-box{
         border-style: solid;
         border-width: 1px;
         height:300px;
     }*/
     #berita{
       height:600px;
       overflow-y: scroll;
       overflow-x:hidden;
     } 
     @media screen and (max-width: 1070px){
        .flex{
    width: 100%;
    grid-template-areas: "artikel";
    grid-template-columns: 1fr;
        }

        .kiri{
            width: 110%;
            margin-left: -5%;
   
        }
        .kanan{
                display: none;
            }
     }
 </style>
<div class="main">
    <h3 style="text-align:center">Selamat Datang Di Portal Kecamatan</h3>
    <div class="container-fluid" style="background-color:white; margin-bottom:10px; padding-bottom:10px; padding-top:10px;">
        <div class="cotent-container">
            <h5>ABOUT 
            <!-- ?php if(isset($_SESSION['akun_id']) AND $_SESSION['akun_level'] == "admin"){
                    echo "<button style='border-radius:8px; margin-left:10px;' class='btn btn-success btn-xs'><i class='fa fa-edit'></i><a href='?page=editAbout' style='color:white'>Ubah</a></button>";}?> -->
                    </h5>
            <hr>
            <div class="content">
            <div class="bg-info p-5 rounded text-white" style="font-size:20px;">
            <b><p style="text-align:center;">Visi kecamatan </p></b>
            <p style="text-align:center; padding-left:5%; padding-right:5%;">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolore, quo! Distinctio quasi magni illum quis quae. Nam quaerat minima tempora doloremque. Cumque sint nobis blanditiis, magni consequuntur accusantium impedit quas?</p>
            <br>
            <b><p style="text-align:center;">Misi kecamatan </p></b>
            <p style="text-align:center; padding-left:5%; padding-right:5%;">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Vero officia animi suscipit neque, omnis labore qui provident maxime iure laudantium, a eius? Repellat vitae qui voluptate odit autem debitis rem!</p></div>
            <br><br>
            <b><p>Sekilas Tentang Kecamatan</p></b>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Provident veritatis, dolor repellendus impedit natus doloremque ratione quasi, placeat illo sunt aut, maiores sint explicabo corrupti hic reprehenderit inventore similique voluptas!</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem culpa aliquid ullam, inventore voluptatibus neque dicta eaque harum accusamus animi nesciunt vitae ab natus voluptates voluptas unde exercitationem, saepe dolor.</p>
            </div>

        </div>
        <div class="content-container">
            <h5>PETUNJUK</h5>
            <hr>
            <div class="content">
                <b><p>Tatacara Pengajuan :</p></b>
                <ol>
                    <li>Jika belum memiliki akun, maka pilih buat akun pada halaman login atau klik <a href="register.php">disini</a></li>
                    <li>Selesaikan proses aktivasi akun pada email yang anda daftarkan</li>
                    <li>Login dengan menggunakan akun yang anda miliki</li>
                    <li>Pilih menu formulir kemudian klik pilihan yang ada sesuai dengan kebutuhan anda</li>
                    <li>Isi formulir yang muncul pada halaman</li>
                    <li>Tekan submit, lalu rekap data akan dikirim ke email anda</li>
                    <li>Pilih menu Data lalu klik pilihan sesuai dengan pengajuan yang telah anda pilih sebelumnya. Pada halaman ini anda dapat melihat status pengajuan anda dan dapat pula melakukan perubahan data jika status pengajuan masih belum disetujui</li>
                    <li>Jika petugas sudah menyetujui atau menolak pengajuan anda, maka laporan akan masuk ke dalam email anda</li>
                    <li>Selalu cek email anda atau cek status pengajuan pada halaman data di website ini</li>
                
                </ol>
            </div>
        </div>
        <!-- <div class="container">
        <h6 style="text-align:center;">Perkembangan Kasus Covid 19</h6>

            <div class="row">
                <div class="col-sm-3 info-box" style="background-color:#ab4438">
                    <b><p>Kasus Nasional</p></b>
                    <div id="indonesia"></div>
                </div>
                <div class="col-sm-3 info-box" style="background-color:#3499eb">
                    <b><p>Kasus Jawa Barat</p></b>
                    <div id="jabar"></div>
                </div>
                <div class="col-sm-3 info-box" style="background-color:#fcba03">
                    <b><p>Kasus Global</p></b>
                    <div id="global"></div>
                </div>
            </div>
        </div> -->


        <div class="content-container" style="margin-bottom:10px;">
                <h5>PENGUMUMAN
                <?php 
                if(isset($_SESSION['akun_username']) AND $_SESSION['akun_level']=="admin"){
                
                    echo "<button style='border-radius:8px; margin-left:10px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i><a href='?page=tulisArtikel' style='color:white'>Tulis Pengumuman</a></button>";}?>
                </h5>
            
            <hr>
            
            <div class="flex">
                <div class="kiri">
                    <div class="kolomartikel">
                <?php
                         foreach($result as $key=>$value){
                            if(isset($_SESSION['akun_username']) AND $_SESSION['akun_level']=="admin"){
                            echo "
                            <div class='col-md-12' style='margin-bottom:30px;'>
                                <div style='border-style:groove; border-width:1px; background-color:#efeaef; border-radius:5px; padding:30px;'>
                                    <h6 style='text-align:center;'><b>".$value['judul']."</b></h6>
                                    <p style='text-align:right;'><small> Tanggal : ".$value['tanggal']."</small></p>
                                    <p>".$value['isi']."</p>

                                    <a id='hapus_data' data-toggle='modal' data-target='#hapus-data' data-id_data='".$value['id_pengumuman']."'"."data-judul='".$value['judul']."'".">
                                    <button style='border-radius:8px;' class='btn btn-danger btn-xxs'><i class='fa fa-delete'></i>Hapus</button>
                                    </a>
                                </div>
                            </div>";}else{
                            echo "
                            <div class='col-md-12' style='margin-bottom:30px;'>
                                <div style='border-style:groove; border-width:1px; background-color:#efeaef; border-radius:20px; padding:30px;'>
                                    <h6 style='text-align:center;'><b>".$value['judul']."</b></h6>
                                    <p style='text-align:right;'><small> Tanggal : ".$value['tanggal']."</small></p>
                                    <p>".$value['isi']."</p>
                                </div>
                            </div>";
                        }}

                        ?>
                        </div>
                </div>
                <div class="kanan">
                    <h4 style="text-align:center;"><u>Berita Terkini</u></h4>
                    <div id="berita"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL HAPUS DATA -->

<div class="modal fade" id="hapus-data" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Anda Yakin?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form_hapus_data" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
        
            <div class="form-group">
                <p>Yakin anda akan menghapus data dengan judul <span name="judul" id="judul"></span></p>
                <!-- <label for="nip">NIP : <p name="nip" id="nip"></p> </label> -->
                <input type="hidden" id="id_data" name="id_data">
                <!-- <input type="text" name="nama" class="form-control" id="nama" required> -->
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Yakin">
        </div>
        </form>
    </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script type="text/javascript">
// HAPUS DATA
$(document).on("click", "#hapus_data", function() {

    var judul = $(this).data('judul');
    var id_data = $(this).data('id_data');
    $("#modal-edit #judul").text(judul);
    $("#modal-edit #id_data").val(id_data);
})

$(document).ready(function(e) {
    $("#form_hapus_data").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'hapus_data_pengumuman.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location='?page=';
    }));
});


// const proxyurl3 = "https://cors-anywhere.herokuapp.com/";
//     const url3 = "https://www.news.developeridn.com/search/?q=covid";
//      $.getJSON(proxyurl3 + url3, function(result){
//        console.log(result.data);
//        $.each(result.data, function(i){
//         //  while(i<=3){

//          document.getElementById("berita").innerHTML +=`
//          <div style="margin: 0 auto; width:auto; margin-bottom:20px;">
//             <a href="${result.data[i].link}" target='_BLANK'>${result.data[i].judul}</a><br>
//             <img src="${result.data[i].poster}"/><br>
//             <small> CNN Indonesia : "${result.data[i].waktu}"</small>
//           </div><hr>`;
//         //  i+=1;
        
//      });
//      });

     $(document).ready(function(){
        // $("button").click(function(){
            $.getJSON("http://newsapi.org/v2/top-headlines?country=id&apiKey=f80c94ea4dd441af82001e601cfb55d4", function(result){
            $.each(result.articles, function(i){
                // console.log(result)
                console.log(result.articles[i].source.name)
                document.getElementById("berita").innerHTML +=`
                <div style="margin: 0 auto; width:auto; margin-bottom:20px;">
                    <a style="font-size:12px;" href="${result.articles[i].url}" target='_BLANK'>${result.articles[i].title}</a><br>
                    <img style="width:200px;"src="${result.articles[i].urlToImage}"/><br>
                    <small style="font-size:10px;"> ${result.articles[i].source.name} : "${result.articles[i].publishedAt}"</small>
                </div><hr>`;
            });
            });
        // });
    });

</script>