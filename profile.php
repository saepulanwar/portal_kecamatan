<style>
    .kotak {
        background-color: #f2f2f0;
        border-radius: 10px;
        display: grid;
        grid-template-areas: "left right other";
        grid-template-columns: 1.5fr 3fr 2fr;
        padding: 20px;
        align-items: center;
        /* height: 100%; */
    }

    .left {
        grid-area: left;

    }

    .left img {
        width: 200px;
        height: 200px;
        border-radius: 50%;
    }

    .right {
        grid-area: right;
        padding-left: 20px;;
    }
    .right td{
        padding-left: 10px;
        padding-top: 20px;
    }

    .other {
        grid-area: other;
        border-left: 2px solid grey;
        padding-left: 20px;
    }

    table {
        width: 100%;
        border-collapse: separate;
        border-spacing: 0 50px;
    }

    .button {
        background-color: #4CAF50;
        /* Green */
        border: none;
        color: white;
        padding: 5px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        /* margin: 4px 2px; */
        cursor: pointer;
        width: 100px;
    }

    .edit {
        background-color: #f2f2f0;
        display: flex;
        margin-top: 50px;
        align-items: center;
        flex-direction: column;
        width: 100%;
        

    }
    @media screen and (max-width: 1070px){
        .kotak {

        grid-template-areas: "left"
                                "right"
                                "other";
            align-items: center;
        width: 100%;
        grid-template-columns: 1fr;


        }

    .left img {
        width: 100px;
        height: 100px;
        border-radius: 50%;
        display: flex;
        justify-content: center;
    }

    .other {
        border-left: none;
        /* border-top: 2px solid grey; */
        margin-top: 50px;
        width: 100%;
    }
    .left {
    display: flex;
    justify-content: center;
    }
        }
</style>
<?php
include "koneksi.php";
$username = $_SESSION['akun_username'];
$sql = "SELECT * FROM user WHERE username='$username'";
// $result = $conn->query($sql);
$data = mysqli_fetch_assoc(mysqli_query($conn, $sql));
$foto = !empty($data['foto']) ? "images/" . $data['foto'] : "assets/img/user.png";
if(isset($_POST['simpan'])){
    $digits = 10;
    if(empty($_FILES['foto']['name'])){
        $foto2 =  $data['foto'];
    }else{
        unlink('images/'.$data['foto']);
        $ext3 = pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);
        $profile = "PROF".rand(pow(10, $digits-1), pow(10, $digits)-1).".".$ext3;
        $source = $_FILES['foto']['tmp_name'];
        $folder = './images/';
        move_uploaded_file($source,$folder.$profile);
        $foto2 =$profile;
    }

    $id=$data['id'];
    $firstname = !empty($_POST['firstname']) ? $_POST['firstname'] : $data['firstname'];
    $lastname = !empty($_POST['lastname']) ? $_POST['lastname'] : $data['lastname'];
    $username = !empty($_POST['username']) ? $_POST['username'] : $data['username'];
    $nik = !empty($_POST['nik']) ? $_POST['nik'] : $data['nik'];
    $email = !empty($_POST['email']) ? $_POST['email'] : $data['email'];
    $tgl_lahir = !empty($_POST['tgl_lahir']) ? $_POST['tgl_lahir'] : $data['tgl_lahir'];
    $tpt_lahir = !empty($_POST['tpt_lahir']) ? $_POST['tpt_lahir'] : $data['tpt_lahir'];
    

    $sql2 = "UPDATE user SET firstname='$firstname', lastname='$lastname', username='$username', nik='$nik', email='$email', foto='$foto2', tgl_lahir='$tgl_lahir', tpt_lahir='$tpt_lahir' WHERE id='$id'";
    $result = $conn->query($sql2);
    header("location:index.php?page=profile");


    
    
    


    
}
?>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<div class="page">
    <di class="kotak">
        <div class="left">
            <div class="image">
            <img src="<?= $foto; ?>" alt="">

            </div>
        </div>
        <div class="right">

            <table>
                <tr>
                    <td style="width:150px;">Nama</td>
                    <td>: </td>
                    <td><?= $data['firstname'] . " " . $data['lastname']; ?></td>
                </tr>
                <tr>
                    <td style="width:150px;">Username</td>
                    <td>: </td>
                    <td><?= $data['username']; ?></td>
                </tr>
                <tr>
                    <td style="width:150px;">Password</td>
                    <td>: </td>
                    <td>***********</td>
                </tr>
                <tr>
                    <td style="width:150px;">NIK</td>
                    <td>: </td>
                    <td><?= $data['nik']; ?></td>
                </tr>
                <tr>
                    <td style="width:150px;">Email</td>
                    <td>: </td>
                    <td><?= $data['email']; ?></td>
                </tr>
                <tr>
                    <td style="width:150px;">Tanggal Lahir</td>
                    <td>: </td>
                    <td><?= $data['tgl_lahir']; ?></td>
                </tr>
                <tr>
                    <td style="width:150px;">Tempat Lahir</td>
                    <td>: </td>
                    <td><?= $data['tpt_lahir']; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><button class="button" onclick="show()">Edit</button></td>
                </tr>


            </table>
        </div>
        <div class="other">
            <form id="form_edit" style="display:none" method="post" action="" enctype="multipart/form-data" name="form">
                <div class="mb-3">
                    <label for="firstname" class="form-label">Nama Depan</label>
                    <input type="text" class="form-control" id="firstname" name="firstname" value="<?= $data['firstname'];?>">
                </div>
                <div class="mb-3">
                    <label for="lastname" class="form-label">Nama Belakang</label>
                    <input type="text" class="form-control" id="lastname" name="lastname" value="<?= $data['lastname'];?>">
                </div>
                <div class="mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="<?= $data['username'];?>">
                    <small id="feedback"></small>
                    <input type="hidden" name="user" value="<?= $data['username'];?>">
                </div>
                <div class="mb-3">
                    <label for="nik" class="form-label">NIK</label>
                    <input type="text" class="form-control" id="nik" name="nik" value="<?= $data['nik'];?>">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="<?= $data['email'];?>">
                </div>
                <div class="mb-3">
                    <label for="tgl_lahir" class="form-label">Tanggal Lahir</label>
                    <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" value="<?= $data['tgl_lahir'];?>">
                </div>
                <div class="mb-3">
                    <label for="tpt_lahir" class="form-label">Tempat Lahir</label>
                    <input type="text" class="form-control" id="tpt_lahir" name="tpt_lahir" value="<?= $data['tpt_lahir'];?>">
                </div>
                <div class="mb-3">
                    <label for="foto" class="form-label">Foto Profile</label>
                    <input type="file" class="form-control" id="foto" name="foto" value="<?= 'images/'.$data['foto'];?>" accept="image/png, image/jpg, image/jpeg">
                </div>

                <button type="submit" name="simpan" class="btn btn-primary" onSubmit = "return checkForm(event)">Simpan</button>
            </form>
        </div>
    </di>
    <!-- <div class="edit">
        <div class="judul">
            <h3>Edit Profile</h3>
        </div>
        <div class="form">
            <form>
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama</label>
                    <input type="text" class="form-control" id="nmaa" name="nama" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username">
                </div>
                <div class="mb-3">
                    <label for="nik" class="form-label">NIK</label>
                    <input type="text" class="form-control" id="nik" name="nik">
                </div>
                <div class="mb-3">
                    <label for="nik" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>

    </div> -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    function validate(){
        alert("ok")
        return false;
    }
    function show(){
        document.getElementById("form_edit").style.display="";
  
    }
    $("form").submit(function(e){
        if($('#feedback').text().includes("sudah"))
        r = confirm("Yakin akan merubah data?");
        if(r == false){
            e.preventDefault(e);
        }
        
    });
    $('#feedback').load('check.php').show();
            $("#username").on("keyup", function() {
                console.log(form.username.value)
                if(form.username.value != form.user.value){
                    $.post('check.php', {
                        username: form.username.value
                    },
                    function(result) {
                        $('#feedback').html(result).show();
                    });
                }else{
                    $('#feedback').hide();
                }
                console.log($('#feedback').text());
            })
</script>