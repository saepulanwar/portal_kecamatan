<style>
    .top {
        display: grid;
        grid-template-areas: "kiri kanan";
        grid-template-columns: 1fr 1fr;
    }

    .kiri {
        grid-area: kiri;
    }

    .kanan {
        grid-area: kanan;
    }
    .text{
        padding-right: 100px;
    }
    .aduan{
        background-color: #fff1b8;
        padding: 10px;
        border-radius: 5px;
        margin-top: 10px;
        font-size: 12px;

    }
    .aduan div{
        display: block;

    }
    .aduan div:hover{
        background-color: #f5e298;
    }

    @media screen and (max-width: 1070px){
        .top {
        grid-template-areas: "kiri"
                                "kanan";
        grid-template-columns: 1fr;
    }
    }
</style>
<?php 
include "koneksi.php";
 if(isset($_POST['simpan'])){
     if(!empty($_POST['judul']) && !empty($_POST['saran'])){
        $judul = $_POST['judul'];
        $konten = $_POST['saran'];
        $username = $_SESSION['akun_username'];
        $sql = "INSERT INTO keluhan(judul, konten, username) VALUES('$judul', '$konten', '$username')";
        $result = $conn->query($sql);
        if($result){
            echo "<script>alert('Berhasil disimpan')</script>";

        }else{
            echo "<script>alert('Gagal disimpan')</script>";

        }
     }else{
         echo "<script>alert('Judul dan Konten Harus Diisi !!')</script>";
     }
 }
?>
<div class="main">
    <div class="top">
        <div class="kiri">
            <div class="text">
                <h3>Petunjuk</h3>
                <ul>
                    <li>Gunakan form di sebelah kanan untuk memberikan keluhan dan saran atau pengaduan</li>
                    <li>Bisa diisikan dengan keluhan anda terhadap pelayanan ataupun saran perbaikan</li>
                    <li>Gunakan bahasa yang sopan untuk kebaikan bersama</li>
                </ul>
            </div>
        </div>
        <div class="kanan">
            <div class="title">
                <h3>Form Pengaduan</h3>
            </div>
            <div class="form">
                <form id="form_edit" method="post" action="" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label for="judul" class="form-label">Judul Keluhan</label>
                        <input type="text" class="form-control" id="judul" name="judul" maxlength="50">
                    </div>
                    <div class="mb-3">
                        <label for="saran" class="form-label">Isi keluhan / Saran</label>
                        <textarea class="form-control" name="saran" id="saran" cols="30" rows="5" maxlength="300"></textarea>
 
                    </div>

                    <button type="submit" name="simpan" class="btn btn-primary" onSubmit="return checkForm(event)">Simpan</button>
                </form>
            </div>
        </div>
    </div>
    <hr>
    <div class="history">
        <h3>History</h3>
        <?php
                $sql = "SELECT * FROM keluhan ORDER BY tanggal desc LIMIT 10";
                $result = $conn->query($sql);
                foreach($result as $key=>$value){?>
        <div class="aduan" onmouseover="this.style.background='#f5e298';" onmouseout="this.style.background='#fff1b8';">
            <p><b><?= $value['username'];?></b> <span><small><?= $value['tanggal'];?></small></span></p>
            <p><?= $value['konten'];?></p>
        </div>
<?php }?>
    </div>
</div>