<?php
ob_start();
session_start();
if(isset($_SESSION['akun_id'])) header("location: index.php");
include "koneksi.php";

if(isset($_POST['submit_login'])){
    $username=$_POST['username'];
    // $pass=hash('sha256',md5($_POST['password']));
    $pass=$_POST['password'];
    $sql_login= mysqli_query($conn, "SELECT * FROM user WHERE username = '$username' AND password='$pass' AND aktif='1'");
    $sql_login2= mysqli_query($conn, "SELECT * FROM user WHERE username = '$username' AND password='$pass' AND aktif='0'");
    $sql_login3= mysqli_query($conn, "SELECT * FROM user WHERE username = '$username'");
    $sql_login4= mysqli_query($conn, "SELECT * FROM user WHERE username = '$username' AND aktif='1'");
    $sql_login5= mysqli_query($conn, "SELECT * FROM user WHERE username = '$username' AND aktif='0'");
    // echo "SELECT * FROM user WHERE username='$username' AND password='$pass'";

// die($sql_login);


    if(mysqli_num_rows($sql_login2)>0 | mysqli_num_rows($sql_login5)>0){
      echo '<div class="alert alert-warning" style="text-align:center;">
      Akun anda belum di aktivasi, silakan <a href="https://mail.google.com" target="_blank">cek email</a> anda
      </div>';
    }else if(mysqli_num_rows($sql_login)>0){
        $row_akun = mysqli_fetch_array($sql_login);
        $_SESSION['akun_id']=$row_akun['id_user'];
        $_SESSION['akun_username']=$row_akun['username'];
        $_SESSION['akun_level']=$row_akun['level'];
        $_SESSION['akun_firstname']=$row_akun['firstname'];
        $_SESSION['akun_lastname']=$row_akun['lastname'];
        $_SESSION['akun_nama']=$row_akun['firstname']." ".$row_akun['lastname'];
        $_SESSION['akun_email']=$row_akun['email'];
        $_SESSION['akun_nik']=$row_akun['nik'];
        $_SESSION['akun_nokk']=$row_akun['no_kk'];
        $_SESSION['akun_foto']=$row_akun['foto'];
        header("location:index.php");
    
    }else if(empty(mysqli_num_rows($sql_login3))){
      echo '<div class="alert alert-warning" style="text-align:center;">
      Anda belum memiliki akun yang terdaftar, silakan <a href="register.php">daftar akun</a>
      </div>';
    }else if(!empty(mysqli_num_rows($sql_login3)) && mysqli_num_rows($sql_login4)>0){
      header("location:login.php?login-gagal");
    // echo "SELECT * FROM user WHERE username='$username'";

    }
  }


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Login - SB Admin</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Login</h3></div>
                                    <div class="card-body">
                                        <form action="" method="post">
                                            <div class="form-floating mb-3">
                                                <input class="form-control" id="username" name="username" type="text" placeholder="Username" />
                                                <label for="username">Username</label>
                                            </div>
                                            <div class="form-floating mb-3">
                                                <input class="form-control" id="password" name="password" type="password" placeholder="Password" />
                                                <label for="password">Password</label>
                                            </div>
                                            <div class="d-flex align-items-center justify-content-between mt-4 mb-0">
                                                <a class="small" href="password.php">Forgot Password?</a>
                                                <button type="submit" name="submit_login" class="btn btn-primary">Login</a>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center py-3">
                                        <div class="small"><a href="register.php">Need an account? Sign up!</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Rancangan Tugas Akhir 2021</div>

                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
