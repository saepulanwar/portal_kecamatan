<?php
ob_start();
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Register - SB Admin</title>
    <link href="css/styles.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body class="bg-primary">
    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <main>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-7">
                            <div class="card shadow-lg border-0 rounded-lg mt-5">
                                <div class="card-header">
                                    <h3 class="text-center font-weight-light my-4">Create Account</h3>
                                </div>
                                <div class="card-body">
                                    <form name="form" action="createuser.php" method="post" enctype="multipart/form-data">
                                        <div class="row mb-3">
                                            <div class="col-md-6">
                                                <div class="form-floating mb-3 mb-md-0">
                                                    <input class="form-control" name="inputFirstName" id="inputFirstName" type="text" placeholder="Enter your first name" required />
                                                    <label for="inputFirstName">First name</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-floating">
                                                    <input class="form-control" name="inputLastName" id="inputLastName" type="text" placeholder="Enter your last name" required />
                                                    <label for="inputLastName">Last name</label>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        if(isset($_SESSION['akun_level'])){
                                        if ($_SESSION['akun_level'] == "admin") {
                                        ?>
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3 mb-md-0">
                                                        <input class="form-control" name="nik" id="nik" type="text" placeholder="Enter your first name" required />
                                                        <label for="nik">NIK</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating">
                                                        <select name="level" id="level" class="form-control">
                                                            <option value="admin">Admin</option>
                                                            <option value="user">User</option>
                                                        </select>
                                                        <label for="level">Level</label>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } }else { ?>
                                            <div class="form-floating mb-3">
                                                <input class="form-control" id="nik" name="nik" type="number" placeholder="name@example.com" required />
                                                <label for="nik">NIK</label>
                                                <small id="feedback3"></small>
                                            </div>
                                            <div class="hidden">
                                                <input type="hidden" name="level" value="user">
                                            </div>
                                        <?php } ?>
                                        <div class="row mb-3">
                                            <div class="col-md-6">
                                                <div class="form-floating mb-3 mb-md-0">
                                                    <input class="form-control" name="tptlahir" id="tptlahir" type="text" placeholder="Enter your Username" required />
                                                    <label for="tptlahir">Tempat Lahir</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-floating">
                                                    <input class="form-control" name="tgllahir" id="tgllahir" type="date" placeholder="Enter your Email" required />
                                                    <label for="tgllahir">Tanggal Lahir</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-md-6">
                                                <div class="form-floating mb-3 mb-md-0">
                                                    <input class="form-control" name="username" id="username" type="text" placeholder="Enter your Username" required />
                                                    <label for="username">Username</label>
                                                    <small id="feedback"></small>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-floating">
                                                    <input class="form-control" name="email" id="email" type="email" placeholder="Enter your Email" required />
                                                    <label for="email">Email</label>
                                                    <small id="feedback2"></small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-md-6">
                                                <div class="form-floating mb-3 mb-md-0">
                                                    <input class="form-control" name="inputPassword" id="inputPassword" type="password" placeholder="Create a password" required />
                                                    <label for="inputPassword">Password</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-floating mb-3 mb-md-0">
                                                    <input class="form-control" id="inputPasswordConfirm" type="password" placeholder="Confirm password" required />
                                                    <label for="inputPasswordConfirm">Confirm Password</label>
                                                    <small id="notifpass" style="color:red">password berbeda</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-floating mb-3">
                                            <input class="form-control" name="foto" id="foto" type="file" accept="image/png, image/jpg, image/jpeg" required>
                                            <label for="foto">Photo Profile</label>
                                        </div>

                                        <div class="mt-4 mb-0">
                                            <div class="d-grid"><button class="btn btn-primary btn-block" type="submit">Create Account</button></div>
                                        </div>
                                    </form>
                                </div>
                                <div class="card-footer text-center py-3">
                                    <div class="small"><a href="login.php">Have an account? Go to login</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div id="layoutAuthentication_footer">
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid px-4">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Rancangan Tugas Akhir 2021</div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script>
        $(document).ready(function() {
            $("#notifpass").hide();
            $("#inputPasswordConfirm").on("keyup", function() {
                var pass = $("#inputPassword").val();
                var conf = $("#inputPasswordConfirm").val();
                if (pass != conf) {
                    $("#notifpass").show();
                } else {
                    $("#notifpass").hide();
                }
            })
            $('#feedback').load('check.php').show();
            $("#username").on("keyup", function() {
                $.post('check.php', {
                        username: form.username.value
                    },
                    function(result) {
                        $('#feedback').html(result).show();
                    });
            })

            $('#feedback2').load('check.php').hide();
            $("#email").on("keyup", function() {
                $.post('check.php', {
                        email: form.email.value
                    },
                    function(result) {
                        $('#feedback2').html(result).hide();
                    });
            })

            $('#feedback3').load('check.php').show();
            $("#nik").on("keyup", function() {
                $.post('check.php', {
                        nik: form.nik.value
                    },
                    function(result) {
                        $('#feedback3').html(result).show();
                    });
            })


        })
    </script>
</body>

</html>