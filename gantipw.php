<?php
 include "koneksi.php";
 $token=$_GET['t'];

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Login - SB Admin</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <?php
                 if(isset($_POST['submit'])){
                    // $sql_cek=mysqli_query($conn,"SELECT * FROM user WHERE token='".$token."'");
                    $sql = "SELECT * FROM user WHERE token='".$token."'";
                    $row = mysqli_fetch_assoc( mysqli_query($conn, $sql) );
                    
                       if(!empty($row)){
                        $id = $row['id'];
                            // $pass1 = $_POST['password'];
                            // $pass2 = $_POST['pass2'];
                            $pass1=hash('sha256',md5($_POST['password']));
                            $pass2=hash('sha256',md5($_POST['password']));
                            if($pass1 == $pass2){
                                $token=hash('sha256', md5(date("Y-m-d h:i:sa"))) ;
                                $sql_update=mysqli_query($conn,"UPDATE user SET password='$pass1', token='$token' WHERE id='".$id."'");
                                if($sql_update){
                                    echo "<script>alert('Password berhasil diubah'); window.location.href='login.php'</script>";

                                }
                            }else{
                                echo '<script>alert("Password yang anda masukan berbeda")</script>';
                            }
                       }else{
                           echo '<script>alert("invalid token")</script>';
                       }
                
                    
                    }
        ?>
    <body class="bg-primary">

        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Login</h3></div>
                                    <div class="card-body">
                                        <form action="" method="post">
                                            <div class="form-floating mb-3">
                                                <input class="form-control" id="password" name="password" type="password" placeholder="Password" />
                                                <label for="password">Password</label>
                                            </div>
                                            <div class="form-floating mb-3">
                                                <input class="form-control" id="pass2" name="pass2" type="password" placeholder="Password" />
                                                <label for="password">Password</label>
                                                <small id="notif" style="color:red;">password beda</small>
                                            </div>
                                            <div class="d-flex align-items-center justify-content-center mt-4 mb-0">
                                        
                                                <button type="submit" name="submit" class="btn btn-primary">Submit</a>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Rancangan Tugas Akhir 2021</div>

                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
            $(document).ready(function(){
                $("#notif").hide();
         
            $("#pass2").on("keyup", function(){
                var pass1 = $("#password").val();
                var pass2 = $("#pass2").val();
                // console.log(pass2)
                if(pass1 != pass2){
                    $("#notif").show();
                }else{
                    $("#notif").hide();
                }
            })
        })
        </script>
    </body>
</html>