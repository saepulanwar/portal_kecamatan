<?php
include "koneksi.php";
$sqlkk = "select *, DATEDIFF(tanggal_proses, tanggal_input) AS hari from buat_kk;";
$resultkk = $conn->query($sqlkk);

$sqlktp = "select *, DATEDIFF(tanggal_proses, tanggal_input) AS hari from data_ktp;";
$resultkt = $conn->query($sqlktp);

$sqlkia = "select *, DATEDIFF(tanggal_proses, tanggal_input) AS hari from data_kia;";
$resultkia = $conn->query($sqlkia);

$sqlsk = "select *, DATEDIFF(tanggal_proses, tanggal_input) AS hari from data_skck;";
$resultsk = $conn->query($sqlsk);

$sqlpk = "select *, DATEDIFF(tanggal_proses, tanggal_input) AS hari from data_spk;";
$resultpk = $conn->query($sqlpk);

$sqlpd = "select *, DATEDIFF(tanggal_proses, tanggal_input) AS hari from data_spd;";
$resultpd = $conn->query($sqlpd);

?>
<!-- <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script> -->
<div class="main">

<span class="row">
                <p class="table-title">Kolom Pencarian</p>
                <div class="search" style="display:flex;">
                    <input style=" margin:10px; float:left;" type="text" id="idData" onkeyup="cariId()" placeholder="Cari ID..." title="Type in a name">
                    <input style=" margin:10px; float:left;" type="text" id="doc" onkeyup="cariDoc()" placeholder="Cari nama..." title="Type in a name">
                </div>
                   
            </span>
    <table class="table table-striped table-hover" id="mytable">
        <tr>
            <th>No</th>
            <th>ID</th>
            <th>Jenis Dokumen</th>
            <th>Status</th>
            <th>Tanggal Pengajuan</th>
            <th>Tanggal Proses</th>
            <th>Interval</th>
        </tr>
        <?php
        $num = 1;
        if ($resultkk->num_rows > 0) {
            // output data of each row
            while ($row = $resultkk->fetch_assoc()) { ?>
                <tr>
                    <td><?= $num; ?></td>
                    <td><?= $row['id_data']; ?></td>
                    <td>Kartu Keluarga</td>
                    <td><?= $row['status']; ?></td>
                    <td><?= $row['tanggal_input']; ?></td>
                    <td><?= $row['tanggal_proses']; ?></td>
                    <td><?= !empty($row['hari']) ? $row['hari'] . " hari" : ""; ?></td>
                </tr>
            <?php $num++;
            }
        }

        if ($resultkt->num_rows > 0) {
            // output data of each row
            while ($row2 = $resultkt->fetch_assoc()) { ?>
                <tr>
                    <td><?= $num; ?></td>
                    <td><?= $row2['id_data']; ?></td>
                    <td>E-KTP</td>
                    <td><?= $row2['status']; ?></td>
                    <td><?= $row2['tanggal_input']; ?></td>
                    <td><?= $row2['tanggal_proses']; ?></td>
                    <td><?= !empty($row['hari']) ? $row['hari'] . " hari" : ""; ?></td>
                </tr>
            <?php $num++;
            }
        }
        if ($resultkia->num_rows > 0) {
            // output data of each row
            while ($row2 = $resultkia->fetch_assoc()) { ?>
                <tr>
                    <td><?= $num; ?></td>
                    <td><?= $row2['id_data']; ?></td>
                    <td>Kartu Identitas Anak</td>
                    <td><?= $row2['status']; ?></td>
                    <td><?= $row2['tanggal_input']; ?></td>
                    <td><?= $row2['tanggal_proses']; ?></td>
                    <td><?= !empty($row['hari']) ? $row['hari'] . " hari" : ""; ?></td>
                </tr>
            <?php $num++;
            }
        }
        if ($resultsk->num_rows > 0) {
            // output data of each row
            while ($row2 = $resultsk->fetch_assoc()) { ?>
                <tr>
                    <td><?= $num; ?></td>
                    <td><?= $row2['id_data']; ?></td>
                    <td>Surat Pengantar SKCK</td>
                    <td><?= $row2['status']; ?></td>
                    <td><?= $row2['tanggal_input']; ?></td>
                    <td><?= $row2['tanggal_proses']; ?></td>
                    <td><?= !empty($row['hari']) ? $row['hari'] . " hari" : ""; ?></td>
                </tr>
            <?php $num++;
            }
        }
        if ($resultpk->num_rows > 0) {
            // output data of each row
            while ($row2 = $resultpk->fetch_assoc()) { ?>
                <tr>
                    <td><?= $num; ?></td>
                    <td><?= $row2['id_data']; ?></td>
                    <td>Surat Pindah Keluar</td>
                    <td><?= $row2['status']; ?></td>
                    <td><?= $row2['tanggal_input']; ?></td>
                    <td><?= $row2['tanggal_proses']; ?></td>
                    <td><?= !empty($row['hari']) ? $row['hari'] . " hari" : ""; ?></td>
                </tr>
        <?php $num++;
            }
        } 
        if ($resultpd->num_rows > 0) {
            // output data of each row
            while ($row2 = $resultpd->fetch_assoc()) { ?>
                <tr>
                    <td><?= $num; ?></td>
                    <td><?= $row2['id_data']; ?></td>
                    <td>Surat Pindah Datang</td>
                    <td><?= $row2['status']; ?></td>
                    <td><?= $row2['tanggal_input']; ?></td>
                    <td><?= $row2['tanggal_proses']; ?></td>
                    <td><?= !empty($row['hari']) ? $row['hari'] . " hari" : ""; ?></td>
                </tr>
        <?php $num++;
            }
        } ?>
    </table>
    <!-- <div class="chart">
    <figure class="highcharts-figure">
    <div id="container"></div>
    <p class="highcharts-description">
        Chart showing how an HTML table can be used as the data source for the
        chart using the Highcharts data module. The chart is built by
        referencing the existing HTML data table in the page. Several common
        data source types are available, including CSV and Google Spreadsheet.
    </p>

    <table id="datatable">
        <thead>
            <tr>
               <th></th>
                <th>KK</th>
                <th>KTP</th>
                <th>KIA</th>
            </tr>
        </thead>
        <tbody>
            <tr>
               <th><5 hari</th>
                <td>3</td>
                <td>4</td>
                <td>4</td>
            </tr>
                        <tr>
               <th><10</th>
                <td>3</td>
                <td>4</td>
                <td>6</td>
            </tr>
           
        </tbody>
    </table>
</figure>
    </div> -->
</div>

<script>

// Highcharts.chart('container', {
//     data: {
//         table: 'datatable'
//     },
//     chart: {
//         type: 'column'
//     },
//     title: {
//         text: 'Waktu Proses'
//     },
//     yAxis: {
//         allowDecimals: false,
//         title: {
//             text: 'Units'
//         }
//     },
//     tooltip: {
//         formatter: function () {
//             return '<b>' + this.series.name + '</b><br/>' + 'proses '+
//                 this.point.name.toLowerCase()+' : '+this.point.y;
//         }
//     }
// });
    function cariId() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("idData");
  filter = input.value.toUpperCase();
  table = document.getElementById("mytable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
function cariDoc() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("doc");
  filter = input.value.toUpperCase();
  table = document.getElementById("mytable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}cariDoc
</script>